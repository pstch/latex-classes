% Code specification LaTeX class
% =============================================================================

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{codespec}[2018/10/10 Code specification class]
\LoadClass[11pt]{article}

% Packages
% -----------------------------------------------------------------------------

\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage[normalem]{ulem}
\RequirePackage[most]{tcolorbox}
\RequirePackage{xcolor,textcomp,makecell,hyperref}
\RequirePackage[nameinlink,capitalize]{cleveref}
\RequirePackage{tabularx,enumitem,graphicx,listings,outlines,tikz}
\RequirePackage[toc,abbreviations,numberedsection,symbols,style=index]{glossaries-extra}

% hyperref configuration
% -----------------------------------------------------------------------------

\hypersetup{
    colorlinks,
    linkcolor={blue!60!black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black}
  }
  
% autoref overrides
% -----------------------------------------------------------------------------

\newcommand{\boxref}[1]{\fbox{\cref{#1}}}
\renewcommand{\autoref}[1]{\boxref{#1}}

% glossary overrides
% -----------------------------------------------------------------------------

\newcommand{\nge}{\newglossaryentry}

% TikZ configuration
% -----------------------------------------------------------------------------
  
\usetikzlibrary{arrows,calc,svg.path}


% tcolorbox configuration
% -----------------------------------------------------------------------------

\newtcolorbox{note}[1][]{%
  enhanced jigsaw, sharp corners, attach title to upper,
  borderline west={2pt}{0pt}{blue}, boxrule=0pt,
  colback=blue!5, coltitle={black},
  fonttitle={\large\bfseries},
  title={Note:\ },
  #1
}

\newtcolorbox{warn}[1][]{
  enhanced jigsaw, sharp corners, attach title to upper,
  borderline west={2pt}{0pt}{red}, boxrule=0pt, 
  colback=red!5, coltitle={black},
  fonttitle={\large\bfseries},
  title={Warning:\ },
  #1
}
